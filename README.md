# README #

This repository contains my solutions to 2021 [Advent of Code](https://adventofcode.com/)
problems. This year I've been solving everything in Python, using, as usual, Jupyter
notebooks for documenting my work and running code. If you don't have Jupyter and don't
want to install it, I have included HTML renderings of my notebooks. Unfortunately
you can't read HTML directly on Bitbucket, you have to clone the repository.

Enjoy.
