# AoC 2021 day 3
## Part 1
Operating on the data represented as lists of strings seems a good idea. Start from reading in both test and competition data sets. Remember to strip endlines from the strings.


```python
testdiag = [l.strip() for l in open("test.txt","rt")]
diag = [l.strip() for l in open("input.txt","rt")]
```

This function solves the first part, by counting bits on successive positions of the list. The results are built as numbers by shifting in `0` or `1` bits, depending on the result of the counting.


```python
def rdiag(diag):
    gamma = epsilon = 0
    pos = 0
    while pos<len(diag[0]):
        n0 = n1 = 0
        gamma = gamma<<1
        epsilon = epsilon<<1
        for d in diag:
            if d[pos]=='0':
                n0 += 1
            else:
                n1 += 1
        if n0<n1:
            gamma += 1
        else:
            epsilon += 1
        pos += 1
    return (gamma,epsilon)
```

Test it


```python
rdiag(testdiag)
```




    (22, 9)



As expected, so find the competition answer


```python
g,e = rdiag(diag)
g,e,g*e
```




    (217, 3878, 841526)



## Part 2
After some short thought (too short maybe) decided to solve it by starting with two copies of the original list, named `olist` and `clist` in the function below, and filter them down until only one element remains on each. Helper function `cntb` counts the number of zeros and ones at a given string position in a list. Yes, I know there is code duplicated there, but we need to be quick in this competition, and monsieurs Copy and Paste are quick (and unforgiving if you are not careful). 


```python
def rdiag2(diag):
    def cntb(list,p):
        n0 = n1 = 0
        for l in list:
            if l[pos] == '0':
                n0 += 1
            else:
                n1 += 1
        return (n0,n1)
    
    olist = clist = diag
    pos = 0
    while pos<len(diag[0]):
        if len(olist) > 1:
            n0,n1 = cntb(olist,pos)
            c = '0' if n0>n1 else '1'
            olist = [o for o in olist if o[pos] == c]
        if len(clist) > 1:
            n0,n1 = cntb(clist,pos)
            c = '0' if n0<=n1 else '1'
            clist = [o for o in clist if o[pos] == c]
        pos += 1
    return (olist[0],clist[0])
        
```

Test it on the test data set.


```python
rdiag2(testdiag)
```




    ('10111', '01010')



And the competition data set yields the following results 


```python
o,c = rdiag2(diag)
o,c
```




    ('010010011001', '111111100110')



What remains to do is a quick Google to figure out how to convert string representation of a binary to a number (hey, I'm to old to remember such obscure features, OK?). Apparently `int` has a second parameter -- the base.


```python
int(o,2) * int (c,2)
```




    4790390


