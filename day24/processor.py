
def runprogram(prg, inp):
    def getarg2(a):
        if a.isalpha():
            return vars[a]
        else:
            return int(a)

    vars = {'w': 0, 'x': 0, 'y': 0, 'z': 0}
    d = [int(i) for i in inp]
    for line in prg:
        instr = line.strip().split(' ')
        op = instr[0]
        a1 = instr[1]
        if len(instr) > 2:
            a2 = getarg2(instr[2])
        if op == 'inp':
            if len(d)>0:
                vars[a1] = d.pop(0)
            else:
                return vars['y'], vars['z']
        elif op == 'add':
            vars[a1] += a2
        elif op == 'mul':
            vars[a1] *= a2
        elif op == 'div':
            vars[a1] = int(vars[a1]/a2)
        elif op == 'mod':
            vars[a1] = vars[a1] % a2
        elif op == 'eql':
            if vars[a1] == a2:
                vars[a1] = 1
            else:
                vars[a1] = 0
        else:
            print("bad instr ",op)
    return vars['z']
